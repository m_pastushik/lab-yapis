#include <list> 
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
  
class Hash 
{ 
    int records;
    std::list<std::pair<unsigned, std::string>> *table; 
public: 
    Hash(int rec) {
        this->records = rec; 
        table = new std::list<std::pair<unsigned, std::string>>[records]; 
    }
    
    void insertID(std::string data){
        int index = hashFunction(data); 
        table[index].push_back(std::make_pair(index, data));         
    }

    void findID(std::string data){
        int search = 0;
        int index = hashFunction(data);
        std::list<std::pair<unsigned, std::string>>::iterator i;
        for(i = table[index].begin(); i != table[index].end(); i++) {
            search++;
            if(i->second == data)
                break;
        }
        if(i != table[index].end())
            std::cout << "ID: " << data << " was found |Hash: " << index << " |Search count:  " << search << '\n';
        else std::cout << "ID: " << data << " was not found\n";

    }
    void deleteID(std::string data){
        int index = hashFunction(data);
        std::list<std::pair<unsigned, std::string>>::iterator i;
        for(i = table[index].begin(); i != table[index].end(); i++) {
            if(i->second == data)
                break;
        }
        if(i != table[index].end())
            table[index].erase(i);
        std::cout << '\n' << data << " was deleted\n";
        displayHash();
    }
    unsigned hashFunction(std::string data) { 
        return (static_cast<unsigned>(data[0]) + static_cast<unsigned>(data[1]))%records; 
    } 
    void displayHash(){
        for (int i = 0; i < records; i++) { 
            std::cout << i; 
        for (auto x : table[i]) 
            std::cout << " --> " << x.second << "(" << x.first << ")"; 
        std::cout << '\n'; 
        } 
    }
    ~Hash(){
        delete[] table;
    }
}; 

int main() 
{ 
    std::vector<std::string> id; 
    std::ifstream fin("D:\\YAPIS\\data.txt");
    while(!fin.eof()){
        std::string temp;
        fin >> temp;
        id.push_back(temp);
    }
    fin.close();
    
    Hash h(id.size()); 
    for (std::string el : id)  
        h.insertID(el);   
    h.displayHash(); 

    h.findID("temp2");
    h.findID("func");

    h.deleteID("temp1");
    return 0; 
}